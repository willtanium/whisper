#include <iostream>
#include <portaudio.h>
#include <stdio.h>
#include <whisper.h>
#define FRAMES_PER_BUFFER (960)
#define SAMPLE_RATE (24000)
#define PA_SAMPLE_TYPE paInt16
#define SAMPLE_SILENCE (0)

class App {
public:
  App(std::string host, int port, std::string user, std::string password) : whisper(),
                                                                            stream(nullptr),
                                                                            host(host),
                                                                            port(port),
                                                                            user(user),
                                                                            password(password),
                                                                            init_audio(false),
                                                                            is_running(false) {
    whisper.on_version_cb([&](uint16_t major, uint8_t minor, uint8_t patch, std::string release, std::string os, std::string os_version) {
      printf("%d:%d:%d REL:%s\n", major, minor, patch, release.c_str());
    });
    whisper.on_codec_version_cb([&](int32_t alpha, int32_t beta, bool prefer_alpha, bool opus) {
      printf("ALPHA %d: BETA %d\n", alpha, beta);
      if (opus)
        printf("IS OPUS");
    });
    whisper.on_audio_cb([&](int target, int session_id, int sequence_number, int16_t *pcm_data, uint32_t pcm_data_size) {
      printf("TARGET:%d SESSION ID:%d SEQ-NO:%d PCM-DATA-SIZE:%d\n", target, session_id, sequence_number, pcm_data_size);
      if (init_audio) {
        Pa_WriteStream(stream, pcm_data, pcm_data_size);
      }
    });
    whisper.on_channel_state_cb([&](std::string name, int32_t channel_id, int32_t parent, std::string description, std::vector<uint32_t> links,
                                    std::vector<uint32_t> links_added, std::vector<uint32_t> links_removed, bool temporary, int32_t position) {
      printf("NAME:%s CHANNEL-ID:%d PARENT:%d DESC:%s\n", name.c_str(), channel_id, parent, description.c_str());
    });
    whisper.on_permission_query_cb([&](int32_t channel_id, uint32_t permissions, int32_t flush) {
      printf("CHANNEL-ID:%d\n FLUSH:%d", channel_id, flush);
    });
    whisper.on_user_state_cb([&](int32_t session, int32_t actor, std::string name, int32_t user_id, int32_t channel_id, int32_t mute, int32_t deaf, int32_t suppress,
                                 int32_t self_mute, int32_t selt_deaf, std::string comment, int32_t priority_speaker, int32_t recording) {
      printf("SESSION:%d ACTOR:%d NAME:%s USER-ID:%d\n", session, actor, name.c_str(), user_id);
    });
    whisper.on_server_config_cb([&](uint32_t max_bandwidth, std::string welcome_text, uint32_t allow_html, uint32_t message_length, uint32_t image_message_length) {
      printf("MAX-BAND-WIDTH:%d\nWELCOME-TEXT:%s", max_bandwidth, welcome_text.c_str());
    });
    whisper.on_server_sync_cb([&](std::string welcome_text, int32_t session, int32_t max_bandwidth, int64_t permissions) {
      printf("SYNC-DONE\n::::\n ---%s---\nSESSION->%d", welcome_text.c_str(), session);
    });
    whisper.on_text_message_cb([&](uint32_t actor, std::vector<uint32_t> sessions, std::vector<uint32_t> channel_ids, std::vector<uint32_t> tree_ids,
                                   std::string message) {
      printf("ACTOR ID->{%d} \n MESSAGE-> {%s}\n", actor, message.c_str());
    });
  }
  void init() {
    init_audio_stream();
  }

  void run() {
    is_running = true;
    while (is_running) {
      whisper.connect(host, port, user, password);
      whisper.run();
    }
  }

  void init_audio_stream() {
    PaError err = Pa_Initialize();
    inputParams.device = Pa_GetDefaultInputDevice();
    if (inputParams.device == paNoDevice) {
      Pa_Terminate();
      return;
    }
    inputParams.channelCount = 2;
    inputParams.sampleFormat = PA_SAMPLE_TYPE;
    inputParams.suggestedLatency = Pa_GetDeviceInfo(inputParams.device)->defaultLowInputLatency;
    inputParams.hostApiSpecificStreamInfo = NULL;
    outputParams.device = Pa_GetDefaultOutputDevice();
    const PaDeviceInfo *outputDeviceInfo = Pa_GetDeviceInfo(outputParams.device);
    outputParams.channelCount = 2;
    outputParams.sampleFormat = PA_SAMPLE_TYPE;
    outputParams.suggestedLatency = PA_SAMPLE_TYPE;
    outputParams.suggestedLatency = outputDeviceInfo->defaultHighOutputLatency;
    err = Pa_OpenDefaultStream(&stream,
                               2,
                               2,
                               paFloat32,
                               SAMPLE_RATE,
                               FRAMES_PER_BUFFER,
                               [](const void *input_buffer,
                                  void *output_buffer,
                                  unsigned long frames_per_buffer,
                                  const PaStreamCallbackTimeInfo *time_info,
                                  PaStreamCallbackFlags status_flags,
                                  void *user_data) -> int {
                                 int finsihed = paContinue;
                                 App *app = static_cast<App *>(user_data);
                                 if (input_buffer != nullptr) {
                                   app->whisper.send_audio_data((int16_t *)input_buffer, frames_per_buffer);
                                 } else if (output_buffer != nullptr) {
                                   // Something can be done about this audio pipe line
                                 }
                                 return finsihed;
                               },
                               this);
    if (err) {
      Pa_Terminate();
      return;
    }
    err = Pa_StartStream(stream);
    if (err) {
      Pa_Terminate();
      return;
    }
    init_audio = true;
  }

  void stop() {
    init_audio = false;
    is_running = false;
  }
  ~App() {
    if (Pa_IsStreamActive(stream)) {
      Pa_CloseStream(stream);
      Pa_Terminate();
    }
  }

private:
  Whisper whisper;
  PaStreamParameters inputParams, outputParams;
  PaStream *stream;
  std::string host, user, password;
  int port;
  bool init_audio;
  bool is_running;
};

int main(int argc, char **argv) {
  std::string host = "am1.cheapmumble.com";
  int port = 2007;
  std::string user = "mobuzi";
  std::string password = "wewilltrythis4communicationnow";
  App app(host, port, user, password);
  app.init_audio_stream();
  app.run();
  return 0;
}