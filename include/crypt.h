#ifndef CRYPT_H
#define CRYPT_H

#include <openssl/aes.h>

class Crypt {

public:
  Crypt();
  ~Crypt();

  unsigned char raw_key[AES_BLOCK_SIZE];
  unsigned char encrypt_iv[AES_BLOCK_SIZE];
  unsigned char decrypt_iv[AES_BLOCK_SIZE];
  unsigned char decrypt_history[0x100];

  unsigned int uiGood;
  unsigned int uiLate;
  unsigned int uiLost;
  unsigned int uiResync;

  unsigned int uiRemoteGood;
  unsigned int uiRemoteLate;
  unsigned int uiRemoteLost;
  unsigned int uiRemoteResync;

  AES_KEY encrypt_key;
  AES_KEY decrypt_key;
  bool bInit;
  bool isValid() const;

  void setKey(const unsigned char *rkey, const unsigned char *eiv, const unsigned char *div);

  void setDecryptIV(const unsigned char *iv);

  void ocb_encrypt(const unsigned char *plain, unsigned char *encrypted, unsigned int len,
                   const unsigned char *nonce,
                   unsigned char *tag);

  void ocb_decrypt(const unsigned char *encrypted, unsigned char *plain, unsigned int len,
                   const unsigned char *nonce,
                   unsigned char *tag);

  bool decrypt(const unsigned char *source, unsigned char *dst, unsigned int crypted_length);

  void encrypt(const unsigned char *source, unsigned char *dst, unsigned int plain_length);
};

#endif // CRYPT_H