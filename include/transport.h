#ifndef TRANSPORT_H
#define TRANSPORT_H
#include <algorithm>
#include <crypt.h>
#include <enums.h>
#include <functional>
#include <iterator>
#include <mumble.pb.h>
#include <string>
#include <task_scheduler.h>
#include <vector>
#include <asio.hpp>
#include <asio/ssl.hpp>

typedef std::function<bool(MessageType, uint8_t *, int, void *)> PC_FN;
typedef std::function<bool(AudioPacketType, uint8_t *, int, void *)> PE_AUDIO_FN;

const int MAX_UDP_LENGTH = 1024;
const int MAX_TCP_LENGTH = 129 * 1024;
using namespace std::literals;
using tsc::TaskScheduler;
using tsc::TaskContext;
using namespace asio;
using namespace asio::ip;

class Transport {
public:
  Transport(io_service &nt_service, bool noUDP, PC_FN fc_cb, PE_AUDIO_FN aud_cb, void *ctx);
  ~Transport();
  int connect(std::string host, int port, std::string user, std::string password);
  void disconnect();
  ConnectionState getConnectionState() {
    return state;
  }

  bool is_udp_active() {
    return udp_active;
  }
  
  void run() {
    net_service.run();
  }
  void update_ping();
  void write_audio_packet(uint8_t *buffer, int length);
  void send_control_message(MessageType type, google::protobuf::Message &message);

private:
  void send_udp_message(uint8_t *buffer, int length);
  void send_ssl_message(uint8_t *buffer, int length);
  void on_ssl_message_received(MessageType type, google::protobuf::Message &message);
  void send_udp_ping();
  void send_ssl_ping();
  void send_version();
  void do_authentication();
  void do_receive_udp();
  void do_receive_ssl();
  void process_message_internal(MessageType type, uint8_t *buffer, int length);
  void process_encoded_audio_packet(uint8_t *buffer, int length);
  void connect_udp();
  void connect_tcp();
  ConnectionState state;
  PC_FN pc_fn;
  PE_AUDIO_FN pe_audio_fn;
  bool udp_active, no_udp;
  Crypt crypt;
  std::string host;
  std::string user;
  std::string password;
  int port;
  TaskScheduler pingScheduler;
  void *context;
  bool is_initialized;
  io_service &net_service;
  ssl::context ssl_context;
  ip::udp::endpoint udp_receiver_endpoint;
  udp::socket udp_socket;
  ssl::stream<tcp::socket> ssl_socket;
  uint8_t *ssl_incoming_buffer;
  uint8_t udp_incoming_buffer[MAX_UDP_LENGTH];
  std::vector<char> data_buffer;
};

#endif // TRANSPORT_H