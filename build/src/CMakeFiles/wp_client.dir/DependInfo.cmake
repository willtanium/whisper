# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/william/Projects/cxx_app/whisper/src/audio.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/audio.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/crypt.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/crypt.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/main.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/main.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/mumble.pb.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/mumble.pb.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/task_scheduler.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/task_scheduler.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/transport.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/transport.cc.o"
  "/home/william/Projects/cxx_app/whisper/src/whisper.cc" "/home/william/Projects/cxx_app/whisper/build/src/CMakeFiles/wp_client.dir/whisper.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "OPT_TLS_GNUTLS"
  "SIO_TLS"
  "_POSIX_C_SOURCE=200112L"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../vendor/asio/asio/include"
  "../include"
  "src"
  "../vendor/asio/asio"
  "../src/include"
  "/usr/local/include"
  "../vendor/opus/opus/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/william/Projects/cxx_app/whisper/build/vendor/opus/CMakeFiles/opus.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
